
select * from diciplina;

select * from professor;

select * from professor_diciplina;


-- 1
select diciplina.ID as 'Chave primaria',
concat(diciplina.Unidade_Curricular,' : ', diciplina.Materia) as UCs 
from diciplina;
-- 2
select concat(diciplina.Unidade_Curricular,' : ',diciplina.Materia) as Ucs 
from diciplina order by ucs desc;

-- 3
select p.id , p.professor as professor , d.Unidade_Curricular as UC from diciplina d
inner join professor_diciplina pd
on d.ID = pd.Disc_ID
inner join professor p 
on pd.Prof_ID = p.ID
where d.Unidade_Curricular = 'UC05';



-- 4
 
select p.Professor as Professor  , concat(d.Unidade_Curricular , ' : ' , d.materia) as diciplina  from professor p 
inner join professor_diciplina pd on p.ID = pd.Prof_ID
inner join diciplina d  on pd.Disc_ID = d.ID ; 


-- 5

select p.Professor as professor  , count(1) as 'UCS Ministradas'
from professor p 
inner join professor_diciplina pd  on p.id = pd.Prof_ID 
inner join diciplina d on pd.Disc_ID = d.ID
group by professor ;


-- 6

select * from (
	select p.professor as professor  , count(1) as UC
	from professor p 
	inner join professor_disciplina pd  on p.id = pd.Prof_ID 
	inner join diciplina d on pd.Disc_ID = d.ID
	group by professor     
) x 
where x.uc =  (

		select max(x.uc) from (
			select count(1)  as UC
			from professor p 
			inner join professor_disciplina pd  on p.id = pd.Prof_ID 
			inner join diciplina d on pd.Disc_ID = d.ID
		group by p.professor ) as x 
        );



-- 7

select * from (
	select p.professor as professor  , count(1) as UC
	from professor p 
	inner join professor_disciplina pd  on p.id = pd.Prof_ID 
	inner join diciplina d on pd.Disc_ID = d.ID
	group by professor     
) x 
where x.uc = 
(
	select min(x.uc) from (
		select count(1)  as UC
		from professor p 
		inner join professor_disciplina pd  on p.id = pd.Prof_ID 
		inner join diciplina d on pd.Disc_ID = d.ID
	group by p.professor ) as x 
);





