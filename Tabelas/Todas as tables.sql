-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: academicoyuri
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `idade` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno`
--

LOCK TABLES `aluno` WRITE;
/*!40000 ALTER TABLE `aluno` DISABLE KEYS */;
INSERT INTO `aluno` VALUES (1,'Ariel','Santos',21),(2,'Anderson','Satoshi',21),(3,'André','Silva',25),(4,'bruna','araujo',NULL),(5,'birobiro','Carlos',19),(6,'carlos','araujo',12),(7,'celso','silva',35),(8,'Camilla','araujo',43),(9,'jonas','dorgas',20),(10,'wesley','silva',15),(11,'douglas','cristo',26),(12,'antero','silva',20),(13,'abigail','lombard',31),(14,'kaun','santos',NULL),(15,'juan','araujo',22),(16,'bella','araujo',27),(17,'ana','nazare',14),(18,'ana claudia','araujo',24),(19,'cris','santos',NULL);
/*!40000 ALTER TABLE `aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletim`
--

DROP TABLE IF EXISTS `boletim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boletim` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Aluno` int(11) NOT NULL,
  `Nota` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletim`
--

LOCK TABLES `boletim` WRITE;
/*!40000 ALTER TABLE `boletim` DISABLE KEYS */;
INSERT INTO `boletim` VALUES (1,1,9.5),(2,1,6.5),(3,1,5.5),(4,2,8.5),(5,2,7.5),(6,2,8.5),(7,3,6.5),(8,3,9.5),(9,3,4.5),(10,4,3.5),(11,4,1.5),(12,4,9.5),(13,5,9.5),(14,5,9.5),(15,5,2.5),(16,6,3.5),(17,6,6.5),(18,6,8.5),(19,7,7.5),(20,7,7.5),(21,7,7.5),(22,8,6.5),(23,8,6.5),(24,8,5.5),(25,9,3.5),(26,9,9),(27,9,7),(28,10,7.5),(29,10,9.5),(30,10,8.5),(31,11,5.5),(32,11,6.5),(33,11,2.5),(34,12,3.5),(35,12,4.5),(36,12,2.5),(37,13,1.5),(38,13,1.5),(39,13,2.5),(40,14,3.5),(41,14,3.5),(42,14,6.5),(43,15,8.5),(44,15,9.5),(45,15,5.5),(46,16,7.5),(47,16,4.5),(48,16,2.5),(49,17,8.5),(50,17,7.5),(51,17,8.5),(52,18,4.5),(53,18,6.5),(54,18,7.5),(55,19,6.5),(56,19,9.5),(57,19,10);
/*!40000 ALTER TABLE `boletim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,'Tecnico em Informatica'),(2,'Tecnico em Logistica'),(3,'Tecnico em Administraçao'),(4,'Tecnico em Seguranca do Trabalho');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diciplina`
--

DROP TABLE IF EXISTS `diciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diciplina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Unidade_Curricular` varchar(15) NOT NULL,
  `Materia` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diciplina`
--

LOCK TABLES `diciplina` WRITE;
/*!40000 ALTER TABLE `diciplina` DISABLE KEYS */;
INSERT INTO `diciplina` VALUES (1,'UC01','Planejar e executar a montagem de computadores'),(2,'UC02','Planejar e executar a instalação de hardware e software para computadores'),(3,'UC03','Planejar e executar a manutenção de computadores'),(4,'UC04','Projeto Integrador Assistente de Suporte e Manutenção de Computadores'),(5,'UC05','Planejar e executar a instalação de redes locais de computadores'),(6,'UC06','Planejar e executar a manutenção de redes locais de computadores'),(7,'UC07','Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)'),(8,'UC08','Projeto Integrador Assistente de Operação de Redes de Computadores'),(9,'UC09','Conceber, analisar e planejar o desenvolvimento de software'),(10,'UC10','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops'),(11,'UC11','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis'),(12,'UC12','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet'),(13,'UC13','Executar teste e implantação de aplicativos computacionais'),(14,'UC14','Desenvolver e organizar elementos estruturais de sites'),(15,'UC15','Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web'),(16,'UC16','Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais');
/*!40000 ALTER TABLE `diciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplina1`
--

DROP TABLE IF EXISTS `disciplina1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(5) NOT NULL,
  `Descricao` varchar(200) NOT NULL,
  `id_disciplina` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_curso` (`id_disciplina`),
  CONSTRAINT `fk_curso` FOREIGN KEY (`id_disciplina`) REFERENCES `cursos` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplina1`
--

LOCK TABLES `disciplina1` WRITE;
/*!40000 ALTER TABLE `disciplina1` DISABLE KEYS */;
INSERT INTO `disciplina1` VALUES (1,'UC01','Planejar e executar a montagem de computadores',1),(2,'UC02','Planejar e executar a instalação de hardware e software para computadores',1),(3,'UC03','Planejar e executar a manutenção de computadores',1),(4,'UC04','Projeto Integrador Assistente de Suporte e Manutenção de Computadores',1),(5,'UC05','Planejar e executar a instalação de redes locais de computadores',1),(6,'UC06','Planejar e executar a manutenção de redes locais de computadores',1),(7,'UC07','Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)',1),(8,'UC08','Projeto Integrador Assistente de Operação de Redes de Computadores',1),(9,'UC09','Conceber, analisar e planejar o desenvolvimento de software',1),(10,'UC10','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops',1),(11,'UC11','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis',1),(12,'UC12','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet',1),(13,'UC13','Executar teste e implantação de aplicativos computacionais',1),(14,'UC14','Desenvolver e organizar elementos estruturais de sites',1),(15,'UC15','Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web',1),(16,'UC16','Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais',1),(17,'TL01','Apoiar as atividades de compra de equipamentos, materiais, produtos e serviços',2),(18,'TL02','Organizar estoques de equipamentos, materiais e produtos',2),(19,'TL03','Projeto Integrador em Logística',2),(20,'AD01','Auxiliar a elaboração, implementação e acompanhamento do planejamento estratégico das organizações',3),(21,'AD02',' Auxiliar na estruturação e operacionalização de projetos',3),(22,'AD03','Auxiliar na elaboração da folha de pagamento',3),(23,'ST01','Monitorar riscos ocupacionais',4),(24,'ST02','Prestar assistência de primeiros socorros',4),(25,'ST03','Projeto Integrador Técnico em Segurança do Trabalho',4);
/*!40000 ALTER TABLE `disciplina1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor`
--

DROP TABLE IF EXISTS `professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Professor` varchar(80) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor`
--

LOCK TABLES `professor` WRITE;
/*!40000 ALTER TABLE `professor` DISABLE KEYS */;
INSERT INTO `professor` VALUES (1,'Francisco'),(2,'Daniel D.'),(3,'Marcelo'),(4,'Daniel'),(5,'Daniel Santiago'),(6,'Patrick'),(7,'Ricky ');
/*!40000 ALTER TABLE `professor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor1`
--

DROP TABLE IF EXISTS `professor1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor1`
--

LOCK TABLES `professor1` WRITE;
/*!40000 ALTER TABLE `professor1` DISABLE KEYS */;
INSERT INTO `professor1` VALUES (1,'Daniel Santiago'),(2,'Thiago Portugal'),(3,'Marcelo Ferreira'),(4,'Marcia Fortuna'),(5,'Jonatan Silva'),(6,'Patrick Cansado');
/*!40000 ALTER TABLE `professor1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor_disciplina`
--

DROP TABLE IF EXISTS `professor_disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor_disciplina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Prof_ID` int(11) NOT NULL,
  `Disc_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor_disciplina`
--

LOCK TABLES `professor_disciplina` WRITE;
/*!40000 ALTER TABLE `professor_disciplina` DISABLE KEYS */;
INSERT INTO `professor_disciplina` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,4),(5,3,5),(6,3,6),(7,3,7),(8,3,8),(9,4,9),(10,5,9),(11,5,10),(12,5,11),(13,5,12),(14,5,13),(15,7,14),(16,6,15);
/*!40000 ALTER TABLE `professor_disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor_disciplina1`
--

DROP TABLE IF EXISTS `professor_disciplina1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor_disciplina1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PROFESSOR` int(11) NOT NULL,
  `ID_DISCIPLINA` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor_disciplina1`
--

LOCK TABLES `professor_disciplina1` WRITE;
/*!40000 ALTER TABLE `professor_disciplina1` DISABLE KEYS */;
INSERT INTO `professor_disciplina1` VALUES (1,3,1),(2,3,2),(3,3,3),(4,5,4),(5,4,5),(6,2,6),(7,2,7),(8,2,8),(9,2,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,6,14),(15,1,15),(16,1,16);
/*!40000 ALTER TABLE `professor_disciplina1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'academicoyuri'
--

--
-- Dumping routines for database 'academicoyuri'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 20:48:40
