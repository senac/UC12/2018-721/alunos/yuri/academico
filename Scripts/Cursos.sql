-- ( Criar curso / Cria coluna id_disciplina/Add constraint/voces que nao te permita criar a constraint
-- exclua ou os atualize/criar os cursos - Tecnologistica - Administracao - Seguranca do trabalho/criar 3 UCs para cada )


create table Cursos(
ID int not null auto_increment primary key,
Nome varchar(150) not null

);
drop table cursos;

select * from cursos;

select * from disciplina1;

alter table disciplina1 add column id_disciplina int not null;

INSERT INTO Curso(nome) values ('tecnico em Informatica');

insert into cursos (nome) values('Tecnico em Informatica');
insert into cursos (nome) values('Tecnico em Logistica');
insert into cursos (nome) values('Tecnico em Administraçao');
insert into cursos (nome) values('Tecnico em Seguranca do Trabalho');



insert into disciplina1 (codigo, Descricao, id_disciplina) values ('TL01','Apoiar as atividades de compra de equipamentos, materiais, produtos e serviços',2);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('TL02','Organizar estoques de equipamentos, materiais e produtos',2);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('TL03','Projeto Integrador em Logística',2);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('AD01','Auxiliar a elaboração, implementação e acompanhamento do planejamento estratégico das organizações',3);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('AD02',' Auxiliar na estruturação e operacionalização de projetos',3);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('AD03','Auxiliar na elaboração da folha de pagamento',3);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('ST01','Monitorar riscos ocupacionais',4);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('ST02','Prestar assistência de primeiros socorros',4);
insert into disciplina1 (codigo, Descricao, id_disciplina) values ('ST03','Projeto Integrador Técnico em Segurança do Trabalho',4);

alter table disciplina1 
add constraint fk_curso
foreign key(id_disciplina) 
references cursos(id);




update disciplina1 set id_disciplina = 1 where id = 1;
update disciplina1 set id_disciplina = 1 where id = 2;
update disciplina1 set id_disciplina = 1 where id = 3;
update disciplina1 set id_disciplina = 1 where id = 4;
update disciplina1 set id_disciplina = 1 where id = 5;
update disciplina1 set id_disciplina = 1 where id = 6;
update disciplina1 set id_disciplina = 1 where id = 7;
update disciplina1 set id_disciplina = 1 where id = 8;
update disciplina1 set id_disciplina = 1 where id = 9;
update disciplina1 set id_disciplina = 1 where id = 10;
update disciplina1 set id_disciplina = 1 where id = 11;
update disciplina1 set id_disciplina = 1 where id = 12;
update disciplina1 set id_disciplina = 1 where id = 13;
update disciplina1 set id_disciplina = 1 where id = 14;
update disciplina1 set id_disciplina = 1 where id = 15;
update disciplina1 set id_disciplina = 1 where id = 16;




select cursos.nome as Curso, concat(disciplina1.codigo,' : ', disciplina1.Descricao)as Disciplinas from cursos
inner join disciplina1
on disciplina1.id_disciplina = cursos.id;
